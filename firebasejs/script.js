// file: script.js
// Initialize Firebase
var firebaseConfig = {
    apiKey: "AIzaSyCtVTwsUhVZMn78gJ4WM7VfBZQu-QEE7g0",
    authDomain: "fir-study-jam-b6548.firebaseapp.com",
    databaseURL: "https://fir-study-jam-b6548.firebaseio.com",
    projectId: "fir-study-jam-b6548",
    storageBucket: "fir-study-jam-b6548.appspot.com",
    messagingSenderId: "75519000044",
    appId: "1:75519000044:web:9c1c07b004e0726d3d94c9",
    measurementId: "G-W8MTWFXLG7"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

//create firebase database reference
var dbRef = firebase.database();
var contactsRef = dbRef.ref('contacts');

//load older conatcts as well as any newly added one...
contactsRef.on("child_added", function(snap) {
  console.log("added", snap.key, snap.val());
  $('#contacts').append(contactHtmlFromObject(snap.val()));
});

//save contact
$('.addValue').on("click", function( event ) {  
    event.preventDefault();
    if( $('#name').val() != '' || $('#email').val() != '' ){
      contactsRef.push({
        name: $('#name').val().replace(/<[^>]*>/ig, ""),
        email: $('#email').val().replace(/<[^>]*>/ig, ""),
        location: {
          city: $('#city').val().replace(/<[^>]*>/ig, ""),
          state: $('#state').val().replace(/<[^>]*>/ig, ""),
          zip: $('#zip').val().replace(/<[^>]*>/ig, "")
        }
      })
      contactForm.reset();
    } else {
      alert('Please fill atlease name or email!');
    }
  });

//prepare conatct object's HTML
function contactHtmlFromObject(contact){
  console.log( contact );
  var html = '';
  html += '<thead><th>Name</th><th>Email<th><th>Location<th></thead>';
    html += '<tbody><tr>';
      html += '<td>'+contact.name+'</td>';
      html += '<td>'+contact.email+'</td>';
      html += '<td>('+contact.location.zip+')'+contact.location.city+', '+contact.location.state+'</td>';
    html += '</tr>';
  html += '</tbody>';
  return html;
}
