<p align="center"><img src="https://secure.meetupstatic.com/photos/event/e/3/c/1/highres_485518305.jpeg"></p>
# firebase-study-jam-season

This repo is for the GDG Sokoto's Firebase Study Jam Series in 2020. For more events and information about GDG SOKOTO, feel free to check out our meetup page (https://www.meetup.com/gdg-sokoto/).

Please feel free to share any resource, thought, experience and question that you may encounter throughout this series.

# Requirements for All Sessions
- Individual app idea to follow along the series.
- Laptop
- Set up Firebase with Firestore
- Choice of Framework. All our discussions will be around that as well.)

# Online Materials
- Get to know Cloud Firestore (https://www.youtube.com/playlist?list=PLl-K7zZEsYLluG5MCVEzXAQ7ACZBCuZgZ)
- Firebase on the Web (https://www.youtube.com/playlist?list=PLl-K7zZEsYLmnJ_FpMOZgyg6XcIGBu2OX)
- Firebase Documentations (https://firebase.google.com/docs/guides/)

# Firebase with Frameworks
- With Vue (https://medium.freecodecamp.org/how-to-build-a-spa-using-vue-js-vuex-vuetify-and-firebase-adding-authentication-with-firebase-d9932d1e4365)
Note that all contents are subject to change.