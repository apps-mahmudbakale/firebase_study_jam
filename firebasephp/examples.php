<?php 
 require 'Engine.php';
 require 'PhpFirebase.php';


//Creating a record


$pfb = new PhpFirebase($pathToSecretKeyJsonFile);
$pfb->setTable('posts');
$pfb->insertRecord([
    'title' => 'Post one',
    'body' => 'Post one contents'
  ], $returnData);
 //The $returnData which is boolean returns inserted data if set to true. Default is false.

// Getting all records
$pfb->getRecords();

// Getting a record. Note: This can only be done via the record id.
$pfb->getRecord(1); 


//To update a record, do this...

// This takes the record ID and any column you want to update.
$pfb->updateRecord(1, [
  'title' => 'Post one edited'
]);


//To delete created record, do this...

 // This takes only the record ID. Deleting all records will be added in Beta-2
 
 $pfb->deleteRecord(1);

 ?>